import Vue from 'vue'
import Router from 'vue-router'
import TweetFeed from '@/components/TweetFeed'
import TweetInput from '@/components/TweetInput'
import TweetProfile from '@/components/TweetProfile'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: {
        TweetInput,
        TweetFeed
      }
    },
    {
      path: '/profile',
      name: 'Profile',
      component: TweetProfile
    }
  ]
})
